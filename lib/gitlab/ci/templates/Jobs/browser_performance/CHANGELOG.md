# Changelog
All notable changes to this job will be documented in this file.

## [0.1.1] - 2022-03-23
* Add docker variables

## [0.1.0] - 2022-03-20
* Initial version
