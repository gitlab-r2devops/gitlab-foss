## Objective

Measures the browser performance of a web page with the Sitespeed.io container, creates a JSON report including the overall performance score for each page, and uploads the report as an artifact. Here is the complete [documentation](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html). 

## How to use it

1. Copy the job URL located in the `Install` part of the right panel and add it inside the `include` list of your `.gitlab-ci.yml` file (see the [quick setup](/use-the-hub/#quick-setup)). You can specify [a fixed version](#changelog) instead of `latest`.
1. If you need to customize the job (stage, variables, ...) 👉 check the [jobs
   customization](/use-the-hub/#jobs-customization)
1. Well done, your job is ready to work ! 😀

## Job details

* Job name: `browser_performance`
* Docker image:
[`docker:20.10.12`](https://hub.docker.com/r/_/docker)
* Default stage: `performance`
* When: `BROWSER_PERFORMANCE_DISABLED != true`

### Variables

| Name | Description | Default |
| ---- | ----------- | ------- |
| `DOCKER_TLS_CERTDIR` <img width=100/> | A general variable for this job <img width=175/>| `` <img width=100/>|
| `SITESPEED_IMAGE` <img width=100/> | Image for the Sitespeed Docker container <img width=175/>| `sitespeedio/sitespeed.io` <img width=100/>|
| `SITESPEED_VERSION` <img width=100/> | The version for the Sitespeed Docker container <img width=175/>| `14.1.0` <img width=100/>|
| `SITESPEED_OPTIONS` <img width=100/> | Additional options for the Sitespeed Docker container <img width=175/>| `` <img width=100/>|
| `CI_KUBERNETES_ACTIVE` <img width=100/> | Activate Kubernetes inside the CI <img width=175/>| `true` <img width=100/>|
| `KUBECONFIG` <img width=100/> | Configuration file for Kubernetes  <img width=175/>| `false` <img width=100/>|
| `KUBERNETES_PORT` <img width=100/> | Port for Kubernetes <img width=175/>| `false` <img width=100/>|

