## Objective

Creates a build of the application using an existing Dockerfile or Heroku buildpacks.

## How to use it

1. Copy the job URL located in the `Install` part of the right panel and add it inside the `include` list of your `.gitlab-ci.yml` file (see the [quick setup](/use-the-hub/#quick-setup)). You can specify [a fixed version](#changelog) instead of `latest`.
1. If you need to customize the job (stage, variables, ...) 👉 check the [jobs
   customization](/use-the-hub/#jobs-customization)
1. Well done, your job is ready to work ! 😀

## Job details

* Job name: `build`
* Docker image:
[`auto-build-image:v1.9.1`](https://hub.docker.com/r/_/auto-build-image)
* Default stage: `build`
* When: `BUILD_DISABLED != true && AUTO_DEVOPS_PLATFORM_TARGET != EC2`

### Variables

| Name | Description | Default |
| ---- | ----------- | ------- |
| `BUILD_DISABLED` <img width=100/> | Disable the job <img width=175/>| `false` <img width=100/>|
| `AUTO_DEVOPS_PLATFORM_TARGET` <img width=100/> | THe platform to target with the build (EC2, S3, ...) <img width=175/>| `` <img width=100/>|

### Artifact

When the job is successful, the build result is available as artifact.
