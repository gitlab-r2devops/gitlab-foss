## Objective

To ensure your project’s code stays simple, readable, and easy to contribute to, analyze your source code quality with this tool

## How to use it

1. Copy the job URL located in the `Install` part of the right panel and add it inside the `include` list of your `.gitlab-ci.yml` file (see the [quick setup](/use-the-hub/#quick-setup)). You can specify [a fixed version](#changelog) instead of `latest`.
1. If you need to customize the job (stage, variables, ...) 👉 check the [jobs
   customization](/use-the-hub/#jobs-customization)
1. Well done, your job is ready to work ! 😀

## Job details

* Job name: `code_quality`
* Docker image:
[`docker:20.10.12`](https://hub.docker.com/r/_/docker)
* Default stage: `test`
* When: `CODE_QUALITY_DISABLED != true`

### Variables

| Name | Description | Default |
| ---- | ----------- | ------- |
| `DOCKER_DRIVER` <img width=100/> | The specific driver for Docker <img width=175/>| `overlay2` <img width=100/>|
| `DOCKER_TLS_CERTDIR` <img width=100/> | The TLS certificate for Docker <img width=175/>| `` <img width=100/>|
| `CODE_QUALITY_IMAGE` <img width=100/> | The Docker image where to run the code_quality package  <img width=175/>| `registry.gitlab.com/gitlab-org/ci-cd/codequality:0.85.26` <img width=100/>|
| `CODE_QUALITY_DISABLED` <img width=100/> | Deactivate the job <img width=175/>| `false` <img width=100/>|
