## Objective

Scan the repository for secrets and prevent the from the exposition before commiting.

## How to use it

1. Copy the job URL located in the `Install` part of the right panel and add it inside the `include` list of your `.gitlab-ci.yml` file (see the [quick setup](/use-the-hub/#quick-setup)). You can specify [a fixed version](#changelog) instead of `latest`.
1. If you need to customize the job (stage, variables, ...) 👉 check the [jobs
   customization](/use-the-hub/#jobs-customization)
1. Well done, your job is ready to work ! 😀

## Job details

* Job name: `secret_detection`
* Docker image:
[`security-products/secrets:3`](https://hub.docker.com/r/_/security-products/secrets)
* Default stage: `test`
* When: `always`

### Variables

| Name | Description | Default |
| ---- | ----------- | ------- |
| `SECURE_ANALYZERS_PREFIX` <img width=100/> | The registry where to retrieve the package<img width=175/>| `registry.gitlab.com/security-products` <img width=100/>|
| `SECRETS_ANALYZER_VERSION` <img width=100/> | The version used for the package<img width=175/>| `3` <img width=100/>|
| `SECRET_DETECTION_EXCLUDED_PATHS` <img width=100/> | Path to exclude from the detection<img width=175/>| `registry.gitlab.com/security-products` <img width=100/>|
| `GIT_DEPTH` <img width=100/> | Variable used by jobs secret_analyzer<img width=175/>| `50` <img width=100/>|
| `SECURE_ANALYZERS_PREFIX` <img width=100/> | The registry where to retrieve the package<img width=175/>| `registry.gitlab.com/security-products` <img width=100/>|

### Artifacts

When the job is successful, the build result is available as artifact.It generate a report in the `gl-secret-detection-report.json` file.
