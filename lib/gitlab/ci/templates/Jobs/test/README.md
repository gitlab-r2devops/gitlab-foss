## Objective

Runs the appropriate tests for your application using Herokuish and Heroku buildpacks by analyzing your project to detect the language and framework.

## How to use it

1. Copy the job URL located in the `Install` part of the right panel and add it inside the `include` list of your `.gitlab-ci.yml` file (see the [quick setup](/use-the-hub/#quick-setup)). You can specify [a fixed version](#changelog) instead of `latest`.
1. If you need to customize the job (stage, variables, ...) 👉 check the [jobs
   customization](/use-the-hub/#jobs-customization)
1. Well done, your job is ready to work ! 😀

## Job details

* Job name: `test`
* Docker image:
[`gliderlabs/herokuish:latest`](https://hub.docker.com/rish/gliderlabs/herokuish)
* Default stage: `test`
* When: `TEST_DISABLED != true`

### Variables

| Name | Description | Default |
| ---- | ----------- | ------- |
| `POSTGRES_VERSION` <img width=100/> | The version of PostgreSQL <img width=175/>| `9.6.16` <img width=100/>|
| `POSTGRES_DB` <img width=100/> | The name of the database <img width=175/>| `9.6.16` <img width=100/>|
| `POSTGRES_USER` <img width=100/> | The name of the DB's user  <img width=175/>| `user` <img width=100/>|
| `POSTGRES_PASSWORD` <img width=100/> | The password associated with the user <img width=175/>| `testing-password` <img width=100/>|
| `$TEST_DISABLED` <img width=100/> | Disabled the job <img width=175/>| `false` <img width=100/>|
